from api.enums.error_code import ErrorCode
import requests


def test_ad_creation():
    response = requests.post('http://localhost:8000/',
                             json={'ad_title': 'Ad title', 'description': 'Ad description', 'cover_image_url': 'http://example.com/test.png', 'price': 150.99, 'images': []})
    assert response.status_code == 200
    assert response.text.split("\r\n")[0].isnumeric()


def test_ad_creation_failure():
    response = requests.post('http://localhost:8000/',
                             json={'ad_title': [], 'description': 95, 'cover_image_url': True, 'price': False, 'images': 'test'})
    assert response.status_code == 400
    assert response.reason == str(ErrorCode.AdCreateError)


def test_ad_creation_failure_with_missing_params():
    response = requests.post('http://localhost:8000/',
                             json={'ad_title': 'Ad title'})
    assert response.status_code == 400
    assert response.reason == str(ErrorCode.ParamNotGiven) % 'description'


def test_ad_fetch():
    response = requests.get('http://localhost:8000/ad?id=1')
    assert response.status_code == 200


def test_ad_fetch_not_found():
    response = requests.get('http://localhost:8000/ad?id=9999999')
    assert response.status_code == 404
    assert response.reason == str(ErrorCode.AdNotFound)


def test_ad_fetch_not_valid_id():
    response = requests.get('http://localhost:8000/ad?id=99$1245')
    assert response.status_code == 400
    assert response.reason == str(ErrorCode.AdInvalidId)


def test_get_one_page():
    response = requests.get('http://localhost:8000?page=1')
    assert response.status_code == 200


def test_get_missing_page():
    response = requests.get('http://localhost:8000?page=10000')
    assert response.status_code == 404
    assert response.reason == str(ErrorCode.EmptyPage)
