from api.ad import Ad, AdEncoder

from api.enums.sort_order import SortOrder

from json import dumps


def get_ads(connection, page, price_sort, date_sort):
    cursor = connection.cursor()
    offset = (10 * page) - 10
    cursor.execute('''
        SELECT
        a.*,
        GROUP_CONCAT(b.image_url)
        FROM Ads as a
        LEFT OUTER JOIN Images as b on a.ad_id = b.ad_id
        GROUP BY a.ad_id
        ORDER BY a.created_at {}, a.price {}
        LIMIT 10 OFFSET {}
    '''.format(date_sort, price_sort, offset))

    ads = cursor.fetchall()
    ad_arr = []
    for ad in ads:
        id, ad_title, description, cover_image_url, price, timestamp, images = ad

        if (images):
            images = images.split(",")

        ad_arr.append(Ad(
            id,
            ad_title,
            description,
            cover_image_url,
            price,
            timestamp,
            images)
        )

    if (len(ad_arr) == 0):
        return None

    return dumps(ad_arr, cls=AdEncoder).encode('utf8')


def get_ad_by_id(connection, id):
    cursor = connection.cursor()
    cursor.execute('''
    SELECT
    a.*,
    GROUP_CONCAT(b.image_url)
    FROM Ads as a
    LEFT JOIN Images as b ON a.ad_id = b.ad_id
    WHERE a.ad_id = ?
            ''', (id,))

    fetched_ad = cursor.fetchone()

    if (set(fetched_ad) == {None}):
        return None

    ad_id, ad_title, description, cover_image_url, price, timestamp, images = fetched_ad

    if (images):
        images = images.split(",")

    return Ad(
        ad_id,
        ad_title,
        description,
        cover_image_url,
        price,
        timestamp,
        images).toJSONB()


def create_ad(
        connection,
        ad_title,
        description,
        cover_image_url,
        price,
        images):

    cursor = connection.cursor()

    try:
        cursor.execute('''
        INSERT INTO Ads(
            ad_title,
            description,
            cover_image_url,
            price)
            VALUES(?, ?, ?, ?)
            ''', (ad_title, description, cover_image_url, price))

        ad_id = cursor.lastrowid

        for image in images:
            cursor.execute('''
                INSERT INTO Images(
                image_url,
                ad_id)
                VALUES(?, ?)
                ''', (image, ad_id,)
            )
            connection.commit()
        return ad_id

    except:
        return None
