import sqlite3


class DatabaseProvider():
    def __init__(self):
        self.__connection = sqlite3.connect('database.db')

    def __del__(self):
        self.__close_connection()

    def get_connection(self):
        return self.__connection

    def __close_connection(self):
        self.__connection.close()
