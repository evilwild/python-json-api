from enum import Enum


class ErrorCode(Enum):
    AdNotFound = 'Ad not found'
    AdInvalidId = 'Id parameter is not valid'
    PageParamNotValid = 'Page parameter is not valid'
    ParamNotGiven = 'No "%s" param given'
    AdCreateError = 'Error while creating ad'
    EmptyPage = 'No ads on that page'

    def __str__(self):
        return str(self.value)
