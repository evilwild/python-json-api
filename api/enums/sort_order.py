from enum import Enum


class SortOrder(Enum):
    DESC = 'DESC'
    ASC = 'ASC'

    def __str__(self):
        return str(self.value)

    @staticmethod
    def getByString(input):
        input = input.lower()
        if (input in ['desc', 'asc']):
            if (input == 'desc'):
                return SortOrder.DESC
            else:
                return SortOrder.ASC
        else:
            # Default
            return SortOrder.DESC
