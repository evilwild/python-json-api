import json


class Ad():
    def __init__(self,
                 id,
                 ad_title,
                 description,
                 cover_image_url,
                 price,
                 timestamp,
                 image_urls=[]):
        self.id = int(id)
        self.ad_title = str(ad_title)
        self.description = str(description)
        self.cover_image_url = str(cover_image_url)
        self.price = float(price)
        self.timestamp = str(timestamp)
        self.image_urls = image_urls

    def toJSON(self):
        return json.dumps(self.__dict__)

    def toJSONB(self):
        return json.dumps(self.__dict__).encode('utf8')


class AdEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Ad):
            return o.__dict__
        return json.JSONEncoder.default(self, o)
