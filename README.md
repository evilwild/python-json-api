# JSON API для сайта объявлений

## Метод получения списка объявлений

- Пагинация, 10 объявлений / страница
- Сортировка:
  - по цене
  - по дате
- API Response:
  - ad_name
  - cover_image_url
  - price

## Метод получения объявления
### Обязательные поля ответа:
- ad_name
- cover_image_url
- price
### Опциональные поля ответа:
- description
- image_urls

## Метод создания объявления:

### API REQUEST:
- ad_name
- description
- cover_image_url
- image_urls
- price

### API RESPONSE:

- ad_id
- result_code


## ERD

![Database ERD](./database.png)

POST Test query
curl localhost:8000 -d '{ "ad_title": "Ad title", "description": "Ad description", "cover_image_url": "https://google.com/cat.png", "price": 1.5, "images": ["https://example.com/1", "https://example.com/2"] }'
